# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 18:31:18 2018

@author: enovi
"""

def main():
    choice        = True
    article1_text = ''
    article2_text = ''
    c             = 'y'
    print('Welcome to Article Similarity Analyzer!')
    while choice == True:
         c = input('Compare articles y/n? ').lower()
         if c == 'y':
             print('Enter the first message.')
             article1_text = get_text()
             print('Enter the second message.')
             article2_text = get_text()
             text_match(article1_text,article2_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    article_text = article_text.lower()
    return article_text

def text_match(article1_text,article2_text):
    matches         = 0
    percnt          = 0
    article1_text   = article1_text.split(' ')
    article2_text   = article2_text.split(' ')
    article1_s      = set(article1_text)
    article2_s      = set(article2_text)
    #stopwords.words('english') from nltk corpus
    common          = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves',
                       'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him',
                       'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its',
                       'itself', 'they', 'them', 'their', 'theirs', 'themselves',
                       'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those',
                       'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have',
                       'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an',
                       'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until',
                       'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against',
                       'between', 'into', 'through', 'during', 'before', 'after', 'above',
                       'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off',
                       'over', 'under', 'again', 'further', 'then', 'once', 'here',
                       'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both',
                       'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no',
                       'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very',
                       's', 't', 'can', 'will', 'just', 'don', 'should', 'now']
    article1_nostop = [word for word in article1_s if word not in common]
    article2_nostop = [word for word in article2_s if word not in common]
    cyr_letters     = ['а', 'с', 'е', 'о']
    
    #prevent divide by zero if no text provided
    if len(article1_s) == 0 or len(article2_s) == 0:
        article1_s = 1
        article2_s = 1
    #count matching words in both sets    
    if len(article1_s) >= len(article2_s):
        for word in article1_s:
            if word in article2_s and word not in common:
                matches += 1
        percnt = 100 * matches / len(article1_nostop)
    elif len(article2_s) >= len(article1_s):
        for word in article2_s:
            if word in article1_s and word not in common:
                matches += 1
        percnt = 100 * matches / len(article2_nostop)
    #detect cyrillic letters
    for word in article1_text:
        for letter in word:
            if letter in cyr_letters:
                print('Cyrillic letters detected in first text.')
    for word in article2_text:
        for letter in word:
            if letter in cyr_letters:
                print('Cyrillic letters detected in second text.')
        
    #print match percentage
    print('Article similarity was {} percent.'.format(percnt))
    
main()